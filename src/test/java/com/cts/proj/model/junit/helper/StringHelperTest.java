package com.cts.proj.model.junit.helper;

import org.junit.Test;

import junit.framework.TestCase;

public class StringHelperTest extends TestCase {
	//creating object for the class
	StringHelper h=new StringHelper();
	//Annotation for unit testing
@Test

public void testTruncateAInFirst2Positions() {
	//compares expected value,actual value
	assertEquals("CD",h.truncateAInFirst2Positions("AACD"));
	
}
public void testTruncateInFirst1Position() {
	assertEquals("CD",h.truncateAInFirst2Positions("ACD"));
}
public void testTruncateIn_NoPosition() {
	assertEquals("CD",h.truncateAInFirst2Positions("CD"));
}

public void testAreFirstAndLastTwoCharactersTheSame_ItReturnsFalse() {
	assertFalse(h.areFirstAndLastTwoCharactersTheSame("ABCD"));
}
public void testAreFirstAndLastTwoCharactersTheSame_ItReturnTrue() {
	assertTrue(h.areFirstAndLastTwoCharactersTheSame("ABAB"));
}
}
