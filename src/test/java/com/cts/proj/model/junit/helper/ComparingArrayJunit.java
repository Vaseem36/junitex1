package com.cts.proj.model.junit.helper;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;

import org.junit.Test;


public class ComparingArrayJunit {
	@Test
	public void ComparingArrays() {
		int[] actual= {4,2,1,7,3};
		int[] expected= {1,2,3,4,7};
		Arrays.sort(actual);
		assertArrayEquals(expected,actual);
	}

}
