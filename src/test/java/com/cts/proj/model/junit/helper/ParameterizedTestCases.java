package com.cts.proj.model.junit.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
@RunWith(Parameterized.class)
public class ParameterizedTestCases {
	
		//creating object for the class
		StringHelper h=new StringHelper();
		private String input;
		private String expectedOutput;
		
		public ParameterizedTestCases(String input, String expectedOutput) {
			super();
			this.input = input;
			this.expectedOutput = expectedOutput;
		}
		@Parameters
		public static Collection<String[]> testCondition1(){
			String expectedOutput[][]= {{"AACD","CD"},{"ACD","CD"},{"CD","CD"}};
			return Arrays.asList(expectedOutput);
		}
		//Annotation for unit testing
	@Test

	public void testTruncateAInFirst2Positions() {
		//compares expected value,actual value
		assertEquals(expectedOutput,h.truncateAInFirst2Positions(input));
		
	}
	
	

	
	}


