package com.cts.proj.model.junit.helper;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class BeforeAfterTest {
	@BeforeClass
	public static void beforeTheClass() {
		System.out.println("before class is executed only once and executes first in the program");
	}
	@Before
	public void Adding() {
		int a=2;
		int b=5;
		System.out.println(a+b);
	}

	@Test
	public void test1() {
		System.out.println("test1 executed");
	}
	@After
	public void Subtract() {
		int x1=5;
		int y1=10;
		System.out.println(x1-y1);
	}
	

	@Test
	public void test2() {
		System.out.println("test2 executed");
	}
	@After
	public void Multiply() {
		int x=5;
		int y=10;
		System.out.println(x*y);
	}
	@AfterClass
	public static void afterTheClass() {
		System.out.println("after class executed only once and executes at last in the program ");
	}
	

	
}
